# Knative Ruby App

A simple "hello world" app to get started with Knative

See the GitLab [Serverless documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.